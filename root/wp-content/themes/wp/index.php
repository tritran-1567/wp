<?php get_header(); ?>
<h1>This is "Top" page</h1>
<div class="l-container">
    <section class="p-top2">
        <div class="c-title2">
            <h2>News</h2>
        </div>
        <div class="c-list6">
            <ul>
                <?php
                    $args = array(
                        'post_type'=>'news',
                        'post_status'=>'publish',
                        'orderby' => 'date',
                        'order'     => 'ASC',
                        'posts_per_page' => 5, 
                    ); ?>
                <?php $my_query = new WP_Query( $args ); ?>
                <?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                <li>
                    <span>
                        <?php echo get_the_date("Y.m.d"); ?></span>
                    <a href="<?php the_permalink() ?>">
                        <?php the_title(); ?></a>
                </li>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
        </div>
    </section>
</div>
<?php get_footer(); ?>