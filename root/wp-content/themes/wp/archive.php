<?php get_header(); ?>
<div class="l-container">
	<div class="p-archive">
		<div class="c-title2">
			<h2>
				<?php wp_title(''); ?>
			</h2>
		</div>

		<div class="c-list6">
			<ul>
				<?php
			$the_query = new WP_Query(
				$query_string.'&posts_per_page=5&paged='.get_query_var('paged')
			);
			if($the_query->have_posts()){while($the_query->have_posts()){$the_query->the_post();
			get_loop_1();
			}}else{echo '<div class="u-noentry">投稿はありません</div>';}
		?>
			</ul>
		</div>

		<?php 
		if(function_exists('wp_pagenavi')) {
			echo '<div class="l-pagenavi">';
			wp_pagenavi(array('query' => $the_query));
			echo '</div>';
		}
		wp_reset_postdata();
	?>
	</div>
</div>
<?php get_footer(); ?>
