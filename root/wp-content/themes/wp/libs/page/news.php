<div class="p-news">
		<div class="c-title2">
			<h2>News archive</h2>
		</div>

		<div class="c-list6">
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$archive_year = get_query_var('year');
            $archive_month = get_query_var('monthnum');
			$query = new WP_Query(
				array(
					'posts_per_page' => 5,
					'post_type'=>'news',
					'paged' => $paged,
					 'year' => $archive_year,
					 'monthnum' => $archive_month,
				)
			);
			?>

			<section class="p-company2">
				<ul>
					<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();?>
						<li><span><?php echo get_the_date('Y/m/d'); ?></span> <a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
					<?php endwhile;endif;  ?>
				</ul>
			</section>
			<?php wp_pagenavi(); wp_reset_query(); ?>
			<?php wp_reset_postdata(); ?>
		</div>
</div>