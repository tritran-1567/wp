<?php if(is_home()){ ?>
<title><?php bloginfo('name'); ?></title>

<?php }else{ ?>
<title><?php echo trim(wp_title('', false)); ?><?php if(!is_home()){ ?> | <?php } ?><?php bloginfo('name'); ?></title>

<?php } ?>
<?php 
    $title = "";
    $description = "";
    $keyword = "";

    if($page_id == "top") {
        
        $title = "Very nice company";
        $description = "It is a very nice company website.";
        $keyword = "very, nice, company";

    } elseif($page_id == "company") {

        $title = "Company - Very nice company";
        $description = 'It is a very nice company website "Company" page.';
        $keyword = "very, nice, company";

    } elseif($page_id == "service") {

        $title = "Service - Very nice company";
        $description = 'It is a very nice company website "Service" page.';
        $keyword = "very, nice, company, service";

    } elseif($page_id == "news") {

        $title = "News - Very nice company";
        $description = 'It is a very nice company website "News" page.';
        $keyword = "very, nice, company, news";

    } elseif($page_id == "access") {

        $title = "Access - Very nice company";
        $description = 'It is a very nice company website "Access" page.';
        $keyword = "very, nice, company, access";

    }
?>
    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keyword; ?>">