<!doctype html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<?php /*=======================================
Meta
===============================================*/ ?>
<?php include(get_template_directory().'/libs/meta.php');?>


<?php /*=======================================
CSS
===============================================*/ ?>
<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css">

<?php /*======================================
JS
===============================================*/ ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-migrate-3.0.1.min.js"></script>

<?php /*======================================
Viewport
===============================================*/ ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<?php /*
<?php
require_once 'libs/Mobile_Detect.php';
$detect = new Mobile_Detect;

//タブレット
if($detect->isTablet()){?>

<?php //スマホ
}elseif($detect->isMobile()){?>

<?php //デスクトップ
}else{ ?>

<?php }?>

*/ ?>


<?php /*======================================
WPhead
===============================================*/ ?>
<?php wp_head(); ?>

</head>

<?php /*=======================================
body ページ別クラス追加
===============================================*/ ?>
<?php
if(is_page('top') || is_home()){
	$class="page-top";
}elseif(is_page()){
	$class="page-".get_page($page_id)->post_name;
}elseif(is_archive()){
	$class="archive-".get_post_type();
}elseif(is_single()){
	$class="single";
	$class.=" single-".get_post_type();
	$class.=" single-".$post->post_name;
}
$class_ie="";

$ua = $_SERVER['HTTP_USER_AGENT'];
if (strstr($ua, 'Trident') || strstr($ua, 'MSIE')) {$class_ie=" ie";}?>

<body id="pagetop" class="<?php echo $class; ?><?php echo $class_ie; ?>">
<header>
	<nav>
		<ul>
			<li><a href="/">Top</a></li>
			<li><a href="/company">Company</a></li>
			<li><a href="/company/service">Service</a></li>
			<li><a href="/news">News</a></li>
			<li><a href="/access">Access</a></li>
		</ul>
	</nav>
</header>