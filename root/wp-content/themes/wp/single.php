<?php get_header(); ?>

<div class="l-container">
	<div class="p-single">
		<time><?php the_time('Y/n/j'); ?></time>
		<div class="c-title2">
			<h3><?php the_title(); ?></h3>
		</div>

		<?php while (have_posts()){ the_post();the_content();} ?>
	</div>
	<nav class="c-navi100">
		<ul>
			<li class="c-navi100__old"><?php previous_post_link( '%link','< %title'); ?></li>
			<li class="c-navi100__new"><?php next_post_link( '%link','%title >'); ?></li>
		</ul>
	</nav>
</div>


<?php get_footer(); ?>
